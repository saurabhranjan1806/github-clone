import React from 'react';

const Context = React.createContext({
  followers: [],
});

export default Context;
