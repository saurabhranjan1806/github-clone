export default function reducer(state, { type, payload }) {
  switch (type) {
    case 'SET_FOLLOWERS': {
      return {
        ...state,
        followers: payload,
      };
    }
    default:
      return state;
  }
}
