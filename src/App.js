import React, { useContext, useReducer, useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Containers/Home';
import Context from './Store/context';
import reducer from './Store/reducer';
// import './App.css';

// const Context = React.createContext({});

function App() {
  const initialState = useContext(Context);
  // console.log({ initialState });
  const [state, dispatch] = useReducer(reducer, { followers: [] });

  return (
    <BrowserRouter>
      <Context.Provider value={{ state, dispatch }}>
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </Context.Provider>
    </BrowserRouter>
  );
}

export default App;

// write an implementation of useReducer with custom hook
// deep understanding of hooks and closures

// typescript side of things
