import React, { useContext, useEffect, useState } from 'react';
import { useQuery } from '@apollo/client';
import styled from 'styled-components';
import { GET_USER } from '../Graphql/Queries/Home';
import User from '../Components/Home/User';
import { BodyStandard, H1 } from '../Components/Common/WeightedText';
import Context from '../Store/context';

const OuterComponent = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 40px;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 2px;
  padding-top: ${(props) => props.marginTop || '0px'};
`;

const LoadMore = styled.button`
  display: flex;
  flex-direction: row;
  padding: 8px 12px;
  border-radius: 4px;
  background-color: #f9f9f9;
  border: 1px solid #ccc;
  align-self: center;
`;

const Home = () => {
  const { dispatch, state } = useContext(Context);
  const { followers } = state;

  // const [afterFollowerId, setAfterFollowerId] = useState(null);
  // const [hasNextPage, setHasNextPage] = useState(true);

  const { loading, error, data, fetchMore } = useQuery(GET_USER, {
    variables: { first: 10, after: null },
    onCompleted: () => {
      dispatch({ type: 'SET_FOLLOWERS', payload: data.viewer.followers.edges });
      // setAfterFollowerId(data.viewer.followers.pageInfo.endCursor);
      // setHasNextPage(data.viewer.followers.pageInfo.hasNextPage);
    },
  });

  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error!</div>;
  console.log({ data });

  const loadMore = () => {
    fetchMore({
      variables: { first: 10, after: data.viewer.followers.pageInfo.endCursor },
      updateQuery: (prev, { fetchMoreResult }) => {
        console.log({ prev, fetchMoreResult });
        if (!fetchMoreResult) return prev;
        dispatch({
          type: 'SET_FOLLOWERS',
          payload: [...followers, ...fetchMoreResult.viewer.followers.edges],
        });
        return {
          ...prev,
          viewer: {
            ...fetchMoreResult.viewer,
            followers: {
              ...fetchMoreResult.viewer.followers,
              pageInfo: fetchMoreResult.viewer.followers.pageInfo,
              edges: [
                ...prev.viewer.followers.edges,
                ...fetchMoreResult.viewer.followers.edges,
              ],
            },
          },
        };
      },
    });
  };

  const { viewer } = data;

  return (
    <OuterComponent>
      <User user={viewer} />
      <Row marginTop="16px">
        <H1>Followers:</H1>
      </Row>
      {followers.length > 0
        ? followers.map((edge) => <User key={edge.node.id} user={edge.node} />)
        : null}
      <LoadMore
        disabled={!viewer.followers.pageInfo.hasNextPage}
        onClick={() => loadMore()}
      >
        <BodyStandard>Load More</BodyStandard>
      </LoadMore>
    </OuterComponent>
  );
};

export default Home;
