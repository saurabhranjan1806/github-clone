import { gql } from '@apollo/client';

export const GET_USER = gql`
  query getUser($first: Int, $after: String) {
    viewer {
      id
      name
      login
      avatarUrl
      company
      bio
      bioHTML
      followers(first: $first, after: $after) {
        pageInfo {
          startCursor
          hasNextPage
          endCursor
          hasPreviousPage
        }
        edges {
          node {
            id
            name
            company
            bio
            avatarUrl
            websiteUrl
          }
        }
      }
    }
  }
`;
