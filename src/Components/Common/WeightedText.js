import styled from 'styled-components';

const Text = styled.p`
  margin: 0;
  font-family: Inter-Medium;
  font-size: 16px;
`;

const H1 = styled(Text)`
  font-family: Inter-Medium;
  font-size: 24px;
  line-height: 29px;
  color: ${(props) => props.color || '#413958'};
`;

const H2Standard = styled(Text)`
  font-family: Inter-Medium;
  font-size: 20px;
  line-height: 24px;
  color: ${(props) => props.color || '#413958'};
`;

const H2Bold = styled(Text)`
  font-family: Inter-Bold;
  font-size: 20px;
  line-height: 24px;
  color: ${(props) => props.color || '#413958'};
`;

const H3 = styled(Text)`
  font-family: Inter-Bold;
  font-size: 18px;
  line-height: 22px;
  color: ${(props) => props.color || '#413958'};
`;

const H4Semibold = styled(Text)`
  font-family: Inter-SemiBold;
  font-size: 16px;
  line-height: 19px;
  color: ${(props) => props.color || '#413958'};
`;

const H4Standard = styled(Text)`
  font-family: Inter-Medium;
  font-size: 16px;
  line-height: 19px;
  color: ${(props) => props.color || '#413958'};
`;

const HeadingStandard = styled(Text)`
  font-family: Inter-Medium;
  font-size: 14px;
  line-height: 17px;
  color: ${(props) => props.color || '#413958'};
`;

const HeadingSemibold = styled(Text)`
  font-family: Inter-SemiBold;
  font-size: 14px;
  line-height: 17px;
  color: ${(props) => props.color || '#413958'};
`;

const HeadingBold = styled(Text)`
  font-family: Inter-Bold;
  font-size: 14px;
  line-height: 17px;
  color: ${(props) => props.color || '#413958'};
`;

const Title = styled(Text)`
  font-family: Inter-SemiBold;
  font-size: 13px;
  line-height: 16px;
  color: ${(props) => props.color || '#413958'};
`;

const BodyStandard = styled(Text)`
  font-family: Inter-Medium;
  font-size: 12px;
  line-height: 15px;
  color: ${(props) => props.color || '#413958'};
`;

const BodyRegular = styled(Text)`
  font-family: Inter-Regular;
  font-size: 12px;
  line-height: 15px;
  color: ${(props) => props.color || '#413958'};
`;

const BodySemibold = styled(Text)`
  font-family: Inter-SemiBold;
  font-size: 12px;
  line-height: 15px;
  color: ${(props) => props.color || '#413958'};
`;

const Label = styled(Text)`
  font-family: Inter-Medium;
  font-size: 11px;
  line-height: 15px;
  color: ${(props) => props.color || '#413958'};
`;

const CaptionStandard = styled(Text)`
  font-family: Inter-Medium;
  font-size: 10px;
  line-height: 12px;
  color: ${(props) => props.color || '#413958'};
`;

const CaptionSemibold = styled(Text)`
  font-family: Inter-SemiBold;
  font-size: 10px;
  line-height: 12px;
  color: ${(props) => props.color || '#413958'};
`;

const CaptionBold = styled(Text)`
  font-family: Inter-Bold;
  font-size: 10px;
  line-height: 12px;
  color: ${(props) => props.color || '#413958'};
`;

const WeightedTextInput = styled.input`
  font-family: Inter-SemiBold;
  font-size: 12px;
  line-height: 15px;
  color: ${(props) => props.color || '#413958'};
  border: 0px;
`;

const WeightedTextInputLarge = styled.input`
  font-family: Inter-Medium;
  flex: 1;
  font-size: 20px;
  line-height: 24px;
  color: ${(props) => props.color || '#413958'};
  border: 0px;
`;

const WeightedTextInputMedium = styled.input`
  font-family: Inter-Medium;
  flex: 1;
  font-size: 16px;
  line-height: 18px;
  color: ${(props) => props.color || '#413958'};
  border: 0px;
`;

export {
  Text,
  H1,
  H2Standard,
  H2Bold,
  H3,
  H4Semibold,
  H4Standard,
  HeadingStandard,
  HeadingSemibold,
  HeadingBold,
  Title,
  BodyStandard,
  BodyRegular,
  BodySemibold,
  Label,
  CaptionStandard,
  CaptionSemibold,
  CaptionBold,
  WeightedTextInput,
  WeightedTextInputLarge,
  WeightedTextInputMedium,
};
