import React from 'react';
import styled from 'styled-components';
import {
  BodySemibold,
  BodyStandard,
  HeadingSemibold,
} from '../Common/WeightedText';

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 2px;
  padding-top: ${(props) => props.marginTop || '0px'};
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
`;

const ProfileImage = styled.img`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  margin-right: 20px;
`;

export default function ({ user }) {
  return (
    <Row key={user.id} marginTop="16px">
      <ProfileImage src={user.avatarUrl} />
      <Col>
        <Row>
          <HeadingSemibold>{`${user.name}`}</HeadingSemibold>
        </Row>
        <Row>
          <BodyStandard>Bio:</BodyStandard>
          <BodySemibold style={{ marginLeft: 4 }}>{user.bio}</BodySemibold>
        </Row>
        <Row>
          <BodyStandard>userName:</BodyStandard>
          <BodySemibold style={{ marginLeft: 4 }}>{user.login}</BodySemibold>
        </Row>
        <Row>
          <BodyStandard>Company:</BodyStandard>
          <BodySemibold style={{ marginLeft: 4 }}>{user.company}</BodySemibold>
        </Row>
      </Col>
    </Row>
  );
}
